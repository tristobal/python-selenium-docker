import time
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait


FROM_LANGUAGE_BUTTON_XPATH = "/html/body/div[3]/main/div[5]/div[2]/section[1]/div[7]/div[2]/div[1]/button[7]"
TO_LANGUAGE_BUTTON_XPATH = "/html/body/div[3]/main/div[5]/div[2]/section[2]/div[8]/div[2]/div[3]/button[6]"
TO_DIV_CONTAINER_XPATH = "/html/body/div[3]/main/div[5]/div[2]/section[2]/div[3]/div[1]"


def get_driver():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--window-size=1920,1080')
    # Comment this line if the script will be executed in Docker
    service = Service(executable_path='./chromedriver')
    return webdriver.Chrome(options=chrome_options, service=service)


def select_languages(driver):
    WebDriverWait(driver, 10).until(
        ec.presence_of_element_located((By.CLASS_NAME, "lmt__textarea_container"))
    )

    from_language_container = driver.find_element(
        by=By.XPATH,
        value="//div[@dl-test='translator-source-lang']")
    from_language_container.click()

    from_language_button = from_language_container.find_element(
        by=By.XPATH,
        value=FROM_LANGUAGE_BUTTON_XPATH)
    from_language_button.click()

    to_language_container = driver.find_element(
        by=By.XPATH,
        value="//div[@dl-test='translator-target-lang']")
    to_language_container.click()

    to_language_button = to_language_container.find_element(
        by=By.XPATH,
        value=TO_LANGUAGE_BUTTON_XPATH)
    to_language_button.click()


def translate(driver, english_word):
    try:
        WebDriverWait(driver, 100).until(
            ec.presence_of_element_located((By.CLASS_NAME, "lmt__textarea_container"))
        )

        from_div_container = driver.find_element(by=By.CLASS_NAME, value='lmt__textarea_container')
        from_textarea = from_div_container.find_element(by=By.TAG_NAME, value="textarea")
        from_textarea.clear()
        from_textarea.send_keys(english_word)

        driver.implicitly_wait(5)

        to_div_container = driver.find_element(
            by=By.XPATH,
            value=TO_DIV_CONTAINER_XPATH)
        to_textarea = to_div_container.find_element(by=By.TAG_NAME, value="textarea")

        time.sleep(2)
        WebDriverWait(to_textarea, 10).until(lambda to_textarea: to_textarea.get_attribute('value').strip() != '')
        return to_textarea.get_attribute('value')

    except TimeoutException as e:
        print("Timed out waiting for page to load")
        return "ERROR"


def clean_input(driver):
    from_div_container = driver.find_element(by=By.CLASS_NAME, value='lmt__textarea_container')
    from_textarea = from_div_container.find_element(by=By.TAG_NAME, value="textarea")
    from_textarea.clear()


def append_new_line(file_name, text_to_append):
    """Append given text as a new line at the end of file"""
    with open(file_name, "a+") as file_object:
        # Move read cursor to the start of file.
        file_object.seek(0)
        # If file is not empty then append '\n'
        data = file_object.read(100)
        if len(data) > 0:
            file_object.write("\n")
        # Append text at the end of file
        file_object.write(text_to_append)


driver = get_driver()
driver.get('https://www.deepl.com/translator')

select_languages(driver)

# words.txt must contain a word or phrases in english one per line
with open("words.txt", "r") as f: 
    for line in f:
        translation = translate(driver, line).strip()
        if "[...]" in translation or len(translation) < 3:
            translation = translate(driver, line).strip()
        print(f"{line.strip()}:\t\t{translation}")

        append_new_line("translations.txt", translation)
        clean_input(driver)

driver.quit()
